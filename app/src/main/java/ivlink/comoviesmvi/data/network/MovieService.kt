package ivlink.comoviesmvi.data.network

import ivlink.comoviesmvi.data.model.MovieModel
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

// Created by IvLink on 2019-05-05.
interface MovieService {
    @GET("/")
    fun getMovieAsync(@Query("t") movieName: String, @Query("apikey") api_key: String = "1d954261"): Deferred<MovieModel>
}