package ivlink.comoviesmvi.data.network

import com.epam.coroutinecache.annotations.Expirable
import com.epam.coroutinecache.annotations.LifeTime
import com.epam.coroutinecache.annotations.ProviderKey
import com.epam.coroutinecache.annotations.UseIfExpired
import ivlink.comoviesmvi.data.model.MovieModel
import kotlinx.coroutines.Deferred
import java.util.concurrent.TimeUnit

// Created by IvLink on 3/22/19.
interface CacheProviders {
    @ProviderKey("MovieKey")
    @LifeTime(value = 2, unit = TimeUnit.MINUTES)
    @Expirable
    @UseIfExpired
    fun getMovieAsync(data: Deferred<MovieModel>): Deferred<MovieModel>
}