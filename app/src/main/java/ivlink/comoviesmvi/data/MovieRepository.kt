package ivlink.comoviesmvi.data

import android.util.Log
import ivlink.comoviesmvi.data.network.MovieService
import ivlink.comoviesmvi.domain.RemoteWrapper
import java.io.IOException

// Created by IvLink on 2019-05-02.
class MovieRepository constructor(
    private val movieService: MovieService
) {

    suspend fun searchMovieAsync(name: String): RemoteWrapper {
        return try {
            val movie = movieService.getMovieAsync(name).await()
            Log.d("MVITag", "movie ${movie.title}")
            RemoteWrapper.RemoteData(movie)
        } catch (ex: IOException) {
            RemoteWrapper.RemoteError(ex)
        }
    }

}