package ivlink.comoviesmvi.data.model

import com.google.gson.annotations.SerializedName

// Created by IvLink on 2019-05-02.
data class MovieModel(
    @SerializedName("Response") val response: Boolean = false,
    @SerializedName("Error") val errorMessage: String? = null,
    @SerializedName("Title") val title: String = "",
    @SerializedName("Poster") val posterUrl: String = "",
    @SerializedName("Ratings") val ratings: List<MovieRating> = emptyList()
) {
    val ratingSummary: String
        get() {
            return ratings.fold("") { summary, msRating -> "$summary\n${msRating.summary}" }
        }
}

data class MovieRating(
    @SerializedName("Source") val source: String,
    @SerializedName("Value") val rating: String
) {

    val summary: String get() = "$rating (${sourceShortName(source)})"

    private fun sourceShortName(ratingSource: String): String {
        return when {
            ratingSource.contains("Internet Movie Database") -> "IMDB"
            ratingSource.contains("Rotten Tomatoes") -> "RT"
            ratingSource.contains("Metacritic") -> "Metac"
            else -> ratingSource
        }
    }
}