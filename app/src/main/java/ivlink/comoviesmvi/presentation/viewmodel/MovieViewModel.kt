package ivlink.comoviesmvi.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.*
import com.bumptech.glide.Glide.init
import ivlink.comoviesmvi.data.model.MovieModel
import ivlink.comoviesmvi.domain.MovieIntention
import ivlink.comoviesmvi.domain.MovieUseCase
import ivlink.comoviesmvi.domain.MovieViewState
import ivlink.comoviesmvi.domain.RemoteWrapper
import ivlink.comoviesmvi.utils.map
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class MovieViewModel(private val movieUseCase: MovieUseCase) : ViewModel() {

    private val _viewState = MutableLiveData<MovieViewState>()
    private val _listModels = MutableLiveData<MutableSet<MovieModel>>()
    val viewState: LiveData<MovieViewState>
        get() = _viewState
    val listModels: LiveData<List<MovieModel>> = _listModels.map {
        it.toList()
    }

    init {
        _viewState.value = MovieViewState.JustLoadedState
        _listModels.value = mutableSetOf()
    }

    fun intent(intent: MovieIntention) {
        Log.d("MVITag", "intent $intent")
        when (intent) {
            is MovieIntention.SearchMovieIntention -> {
                onSearchMovie(intent.searchedMovieTitle)
            }
            is MovieIntention.AddToListIntention -> {
                val set = _listModels.value
                val isAdded = set?.add(intent.movie)
                if (isAdded!!)
                    _viewState.value = MovieViewState.AddedToListState(intent.movie)
                _listModels.value = set
            }
            is MovieIntention.RestoreFromListIntention -> {
                _viewState.value = MovieViewState.DataState(intent.movieFromList)
            }
        }
    }

    private fun onSearchMovie(name: String) {
        _viewState.value = MovieViewState.LoadingState
        viewModelScope.launch(Dispatchers.IO) {
            when (val wrapper = movieUseCase.searchMovieAsync(name)) {
                is RemoteWrapper.RemoteData -> {
                    viewModelScope.launch(Dispatchers.Main) {
                        _viewState.value = MovieViewState.DataState(wrapper.model)
                    }
                }
                is RemoteWrapper.RemoteError -> {
                    viewModelScope.launch(Dispatchers.Main) {
                        _viewState.value = MovieViewState.ErrorState(Throwable(wrapper.exception.message))
                    }
                }
            }
        }
    }

    @ExperimentalCoroutinesApi
    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}



