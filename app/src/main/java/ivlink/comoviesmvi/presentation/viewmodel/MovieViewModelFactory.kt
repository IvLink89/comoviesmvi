package ivlink.comoviesmvi.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ivlink.comoviesmvi.domain.MovieUseCase

// Created by IvLink on 2019-05-03.
class MovieViewModelFactory(
    private val useCase: MovieUseCase
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MovieViewModel(useCase) as T
    }
}