package ivlink.comoviesmvi.presentation.di

import androidx.lifecycle.ViewModelProviders
import ivlink.comoviesmvi.BuildConfig
import ivlink.comoviesmvi.data.MovieRepository
import ivlink.comoviesmvi.data.network.RemoteServiceFactory.makeRemoteService
import ivlink.comoviesmvi.domain.MovieUseCase
import ivlink.comoviesmvi.presentation.MainActivity
import ivlink.comoviesmvi.presentation.viewmodel.MovieViewModel
import ivlink.comoviesmvi.presentation.viewmodel.MovieViewModelFactory
import org.koin.dsl.module

// Created by IvLink on 2019-05-03.
val movieModule = module {

    single { MovieRepository(makeRemoteService(BuildConfig.DEBUG)) }
    single { MovieUseCase(get()) }
    factory { (activity: MainActivity) ->
        ViewModelProviders.of(activity, MovieViewModelFactory(get())).get(MovieViewModel::class.java)
    }
}

val movieModuleApp = listOf(movieModule)