package ivlink.comoviesmvi.presentation.adapter

import android.annotation.SuppressLint
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import ivlink.comoviesmvi.R
import ivlink.comoviesmvi.data.model.MovieModel
import ivlink.comoviesmvi.utils.growShrink

// Created by IvLink on 2019-05-03.

class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val posterView: ImageView = itemView.findViewById(R.id.ms_result_poster)
    private val ratingView: TextView = itemView.findViewById(R.id.ms_result_rating)
    private val spinner: CircularProgressDrawable by lazy {
        val circularProgressDrawable = CircularProgressDrawable(itemView.context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()
        circularProgressDrawable
    }

    @SuppressLint("SetTextI18n")
    fun bind(item: MovieModel, historyClickListener: (MovieModel) -> Unit) {
        (item.ratings.first()).let {
            ratingView.text = it.summary
        }
        item.posterUrl.takeIf { it.isNotBlank() }
            ?.let {
//                val options = RequestOptions()
//                options.placeholder(spinner)
//                val builder = GlideBuilder()
//                builder.setDefaultRequestOptions(options)

                Glide.with(posterView.context)
                    .load(it)
                    .into(posterView)
            } ?: run {
            posterView.setImageResource(0)
        }
        itemView.setOnClickListener {
            historyClickListener.invoke(item)
            posterView.growShrink()
        }
    }
}