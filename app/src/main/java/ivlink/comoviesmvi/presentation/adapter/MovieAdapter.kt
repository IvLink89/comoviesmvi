package ivlink.comoviesmvi.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ivlink.comoviesmvi.R
import ivlink.comoviesmvi.data.model.MovieModel
import ivlink.comoviesmvi.utils.inflate

// Created by IvLink on 2019-05-02.
class MovieAdapter(
    private val historyClickListener: (MovieModel) -> Unit
) : ListAdapter<MovieModel, MovieViewHolder>(MovieDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(parent.inflate(R.layout.movie_list_item))
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(getItem(position), historyClickListener)
    }
}

class MovieDiffCallback : DiffUtil.ItemCallback<MovieModel>() {
    override fun areItemsTheSame(oldItem: MovieModel, newItem: MovieModel): Boolean {
        return true
    }

    override fun areContentsTheSame(oldItem: MovieModel, newItem: MovieModel): Boolean {
        return oldItem.posterUrl.equals(newItem.posterUrl, ignoreCase = true)
    }
}


