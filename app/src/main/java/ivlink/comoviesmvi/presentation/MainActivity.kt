package ivlink.comoviesmvi.presentation

import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import ivlink.comoviesmvi.R
import ivlink.comoviesmvi.data.model.MovieModel
import ivlink.comoviesmvi.domain.MovieIntention
import ivlink.comoviesmvi.domain.MovieViewState
import ivlink.comoviesmvi.presentation.adapter.MovieAdapter
import ivlink.comoviesmvi.presentation.viewmodel.MovieViewModel
import ivlink.comoviesmvi.utils.growShrink
import ivlink.comoviesmvi.utils.visible
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf


class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    lateinit var adapter: MovieAdapter
    val movieViewModel: MovieViewModel by inject { parametersOf(this@MainActivity) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initUI()
        setUpIntentions()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        val item = menu?.findItem(R.id.app_bar_search)
        val searchView = item?.actionView as SearchView
        searchView.setOnQueryTextListener(this)
        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        movieViewModel.intent(MovieIntention.SearchMovieIntention(query!!))
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }

    private fun initUI() {
        val layoutManager = LinearLayoutManager(this, HORIZONTAL, false)
        rvList.layoutManager = layoutManager
        adapter = MovieAdapter {
            movieViewModel.intent(MovieIntention.RestoreFromListIntention(it))
        }
        rvList.adapter = adapter
    }

    private fun setUpIntentions() {
        movieViewModel.viewState.observe(this, Observer {
            render(it)
        })
        movieViewModel.listModels.observe(this, Observer {
            adapter.submitList(it)
        })
        ivPoster.setOnClickListener {
            ivPoster.growShrink()
            movieViewModel.intent(MovieIntention.AddToListIntention(ivPoster.getTag(R.id.data_tag) as MovieModel))
        }
    }

    private fun render(state: MovieViewState) {
        Log.d("MVITag", "state $state")
        when (state) {
            is MovieViewState.JustLoadedState -> {
                progressBar.visible(false)
            }
            is MovieViewState.LoadingState -> {
                renderLoading()
            }
            is MovieViewState.DataState -> {
                renderData(state.data)
            }
            is MovieViewState.ErrorState -> {
                renderError(state)
            }
            is MovieViewState.AddedToListState -> {
                renderAddedToList(state.data)
            }
        }
    }

    private fun renderLoading() {
        progressBar.visible()
    }

    private fun renderAddedToList(model: MovieModel) {
        Snackbar.make(content, "Added:${model.title}", Snackbar.LENGTH_LONG)
            .show()
    }

    private fun renderData(model: MovieModel) {
        progressBar.visible(false)
        tvTitle.text = model.title
        tvRating.text = model.ratingSummary
        ivPoster.setTag(R.id.data_tag, model)

        model.posterUrl.takeIf { it.isNotBlank() }
            ?.let {
                Glide.with(this)
                    .load(model.posterUrl)
                    .into(ivPoster)
            } ?: run {
            ivPoster.setImageResource(0)
        }
    }

    private fun renderError(state: MovieViewState.ErrorState) {
        progressBar.visible(false)
        Snackbar.make(content, "Error:\n${state.error.message}", Snackbar.LENGTH_LONG)
            .show()
    }
}
