package ivlink.comoviesmvi.domain

import ivlink.comoviesmvi.data.MovieRepository

// Created by IvLink on 2019-05-03.
class MovieUseCase(private val repository: MovieRepository) {

    suspend fun searchMovieAsync(name: String):RemoteWrapper{
        return repository.searchMovieAsync(name)
    }
}