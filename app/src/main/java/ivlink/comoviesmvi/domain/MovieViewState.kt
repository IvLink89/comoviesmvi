package ivlink.comoviesmvi.domain

import ivlink.comoviesmvi.data.model.MovieModel

// Created by IvLink on 2019-05-02.
sealed class MovieViewState {
    object JustLoadedState : MovieViewState()
    object LoadingState : MovieViewState()
    data class AddedToListState(val data: MovieModel) : MovieViewState()
    data class DataState(val data: MovieModel) : MovieViewState()
    data class ErrorState(val error: Throwable) : MovieViewState()
}