package ivlink.comoviesmvi.domain

import ivlink.comoviesmvi.data.model.MovieModel
import java.lang.Exception

// Created by IvLink on 2019-05-04.
sealed class RemoteWrapper {
    data class RemoteData(val model: MovieModel) : RemoteWrapper()
    data class RemoteError(val exception: Exception) : RemoteWrapper()
}