package ivlink.comoviesmvi.domain

import ivlink.comoviesmvi.data.model.MovieModel

// Created by IvLink on 2019-05-02.
sealed class MovieIntention {
    data class SearchMovieIntention(val searchedMovieTitle: String = "") : MovieIntention()
    data class AddToListIntention(val movie: MovieModel) : MovieIntention()
    data class RestoreFromListIntention(val movieFromList: MovieModel) : MovieIntention()
}