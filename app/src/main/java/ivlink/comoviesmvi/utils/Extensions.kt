package ivlink.comoviesmvi.utils

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.content.BroadcastReceiver
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import android.widget.ImageView
import androidx.annotation.LayoutRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import ivlink.comoviesmvi.R

// Created by IvLink on 2019-05-03.
fun View.visible(visible: Boolean = true) {
    if (visible) this.visibility = VISIBLE
    else this.visibility = GONE
}

fun ViewGroup.inflate(
    @LayoutRes layoutRes: Int,
    attachToRoot: Boolean = false
): View = LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)

fun ImageView.growShrink() {
    val expansionFactor: Float = 0.2F
    val growX = PropertyValuesHolder.ofFloat(View.SCALE_X, 1f + expansionFactor)
    val growY = PropertyValuesHolder.ofFloat(View.SCALE_Y, 1f + expansionFactor)
    val growAnimation = ObjectAnimator.ofPropertyValuesHolder(this, growX, growY)
    growAnimation.interpolator = OvershootInterpolator()

    val shrinkX = PropertyValuesHolder.ofFloat(View.SCALE_X, 1f)
    val shrinkY = PropertyValuesHolder.ofFloat(View.SCALE_Y, 1f)
    val shrinkAnimation = ObjectAnimator.ofPropertyValuesHolder(this, shrinkX, shrinkY)
    shrinkAnimation.interpolator = OvershootInterpolator()

    val animSetXY = AnimatorSet()
    animSetXY.playSequentially(growAnimation, shrinkAnimation)
    animSetXY.start()
}

fun <T, R> LiveData<T>.map(block: (T) -> R): LiveData<R> {
    return Transformations.map(this) {
        block(it)
    }
}